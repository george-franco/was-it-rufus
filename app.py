import git
import pytz
from datetime import datetime


git_dir = git.Repo()
now = datetime.today()
now = pytz.timezone('UTC').localize(now)


def wasItRufus(git_dir):
    print("Active branch:", git_dir.active_branch)
    if len(git_dir.untracked_files) == 0:
        print("Local Changes:", False)
    else:
        print("Local Changes:", True)
    print("Recent Commit", (now - git_dir.head.commit.authored_datetime).days < 7)
    print("Blame Rufus", str(git_dir.head.commit.author) == "Rufus")


wasItRufus(git_dir)