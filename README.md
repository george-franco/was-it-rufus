# was-it-rufus
Coderbyte assessment for Pronto Ai. 

# Installation Instructions
1. Clone the repository down to your local machine
2. CD into the new project directory
3. pip install git
4. pip install gitpython
5. pip install pytz

# Documentation
Learned about the GitPython library through the documentation. Used the following sources to create the 'Was it Rufus' application: https://gitpython.readthedocs.io/en/stable/reference.html#module-git.repo.base & https://gitpython.readthedocs.io/en/stable/tutorial.html#